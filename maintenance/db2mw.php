<?php
/**
 * Copyright (C) 2010 Matthias Meßmer
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * -------------------
 * 
 * This script converts a manual of a KDE applications in docbook format
 * to MediaWiki text to be imported to Userbase Wiki.
 * 
 * importFromString function is an almost 1:1 copy of Rob Church's importTextFile.php
 *
 * References:
 *   Docbook Docu:
 *     http://www.docbook.org/tdg/en/html/docbook.html
 *   Userbase Typographical Guidelines:
 *     http://userbase.kde.org/Typographical_Guidelines
 *   The KDE DocBook Authors guide:
 *     http://l10n.kde.org/docs//markup/index.html
 *
 * @file
 * @ingroup Maintenance
 * @author Rob Church <robchur@gmail.com>
 */

$options = array( 'help', 'nooverwrite', 'norc', 'dry', 'noimages', 'skip-dupes' );
$optionsWithArgs = array( 'user', 'comment', 'customization', 'image-dir', 'extensions', 'comment-ext' );
require_once( 'commandLine.inc' );
require_once( 'importImages.inc' );
require_once( 'db2mw.inc' );

if( count( $args ) < 1 || isset( $options['help'] ) ) {
   showHelp();
   die();
}

function showHelp() {
print <<<EOF
USAGE: php db2mw.php <options> <filename>

<filename> : Path to the index.docbook to import

Common Options:

--user <user>
   User to be associated with the edit
--comment <comment>
   Edit summary
--customization <path>
   Path to KDE docbook dtd customizations
   default is /usr/share/apps/ksgmltools2/customization
--nooverwrite
   Don't overwrite existing content
--norc
   Don't update recent changes
--dry
   Dry run, do not import anything
--help
   Show this information

Image import options:

--image-dir <dir>
   If different from docbook folder
--noimages
   Skip import images
--extensions <exts>  
   Comma-separated list of allowable extensions, defaults to \$wgFileExtensions
--skip-dupes
   Skip images that were already uploaded under a different name (check SHA1)
--comment-ext <ext>
   Causes the comment for each file to be loaded from a file with the same name
   but the extension <ext>. 
   If a global comment is also given, it is appended.
EOF;
}

// Manual index file
$index_docbook = $args[ 0 ];
$dir = dirname( $index_docbook );
chdir( $dir );

echo "Using ".$index_docbook."...\n\n";

if( !is_file( $index_docbook ) )
   die( "does not exists.\n" );
   
// read the index.docbook into a string
// should not be too long
$xml_string = file_get_contents( $index_docbook );

$reader = new DBReader( $xml_string, $options );
$reader->run();

?>